# Photo Organizer

## Description
This is a simple script to organize your photos. I built it to organize photos for the Google Pixel Phone. It takes the standard file format and puts them in folders by year and month. 

## Video
Usage video can be found at: https://youtu.be/M0Pu68srZoI

## Installation
This is built for Jupyter notebooks. You just have to download the file and open it up with your Jupyter notebook.

## Authors and acknowledgment
MadTc

## License
Use it as you see fit.

## Project status
Completed
